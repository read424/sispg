'use strict';
angular.module('BlurAdmin', [
        'ngAnimate',
        'ui.bootstrap',
        'ui.sortable',
        'ui.router',
        'ngTouch',
        'toastr',
        'smart-table',
        'xeditable',
        'ui.slimscroll',
        'ngStorage',
        'permission',
        'permission.ui',
        'angular-progress-button-styles',
        'BlurAdmin.authService',
        'BlurAdmin.printService',
        'BlurAdmin.commonservice',
        'BlurAdmin.getservice',
        'BlurAdmin.facturaservice',
        'BlurAdmin.sides',
        'BlurAdmin.signin',
        'BlurAdmin.theme',
        'BlurAdmin.pages',
        'ngFileUpload', // added for file uploads s3 backet
        'BlurAdmin.theme.components',
        'angular-loading-bar',
        'moment-picker',
        'ui.toggle',
        'ui.select',
        'ngSanitize'
    ])
    .constant('apiEndpoint', '/api/')
    .config(['cfpLoadingBarProvider', '$urlRouterProvider', '$httpProvider', function(cfpLoadingBarProvider, $urlRouterProvider, $httpProvider) {
        cfpLoadingBarProvider.includeSpinner = false;
        $urlRouterProvider
            .otherwise('/signin');
        $httpProvider.interceptors.push(['$q', '$location', '$localStorage', function($q, $location, $localStorage){
            return {
                'request': function(config){
                    config.headers=config.headers || {};
                    if($localStorage.token){
                        config.headers.Authorization='Bearer '+$localStorage.token;
                    }
                    return config;
                },
                'response':function(response){
                    if(response.data.token!==undefined){
                        $localStorage.token=response.data.token;}
                    return response;
                },
                'responseError': function(response){
                    if(response.status===401 || response.status==403){
                        $location.path('/sigin');
                    }
                    return $q.reject(response);
                }
            }
        }])
    }])
    .run(['$rootScope', '$state', 'PermRoleStore', 'PermPermissionStore', 'AuthenticationService', 'serviceAPI', function($rootScope, $state, PermRoleStore, PermPermissionStore, AuthenticationService, serviceAPI) {
        PermRoleStore.defineRole('AUTHORIZED', function(){
            return AuthenticationService.isLoggedIn();
        });
        
        PermRoleStore.defineRole('ADMIN', function(){
            return AuthenticationService.isAdmin();
        });

        PermRoleStore.defineRole('CAJERO', function(){
            return AuthenticationService.isCajero();
        });

        PermRoleStore.defineRole('SUPERVISOR', function(){
            return AuthenticationService.isSupervisor();
        });

        PermRoleStore.defineRole('SUPERUSUARIO', function(){
            return AuthenticationService.isSuperusuario();
        });

        $rootScope.$on('$stateChangeStart', function(evt, to, params, from){
            if((from.name=='signin' && to.name=='dashboard') && params.defaultState!=''){
                evt.preventDefault();
                $state.go(params.defaultState, {}, { location: 'replace' });
            }
            if(to.name=='dashboard.ventas.cajas'){
                serviceAPI.deleteVentaTMP().success(function(r){});
            }
        });
        /*
        $rootScope.$on('$stateChangePermissionDenied', function(){
            console.log('error');
        });
        */
}]);
