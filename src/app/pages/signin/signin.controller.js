(function() {
    'use strict';

    angular.module('BlurAdmin.signin')
        .controller('SignInCtrl', ['$scope', '$state', 'AuthenticationService', 'toastr', 'serviceAPI', 'commonService', function($scope, $state, AuthenticationService, toastr, serviceAPI, commonService){
            $scope.data={username:'', clave:''};
            $scope.signIn = function() {
                serviceAPI.signIn($scope.data).success(function(r){
                    AuthenticationService.setLoggedIn(r.auth);
                    if(r.auth){
                        serviceAPI.infoEmpresa().success(function(r){
                            if(r.success && r.num_rows==1){ commonService.setEmpresa(r.data);}
                        });
                        serviceAPI.getConfig().success(function(r){
                            if(r.success && r.num_rows==1){ commonService.setConfig(r.data);}
                        });
                        AuthenticationService.setRol(r.rows.nom_rol);
                        AuthenticationService.setUser($scope.data.username);
                        AuthenticationService.setToken(r.token);
                        AuthenticationService.setStates(r.rows.states);
                        $state.go('dashboard', {defaultState: r.rows.name_state});
                    }else{
                        toastr.error("Datos de acceso inválidos", 'Error');
                    }
                });
            };  
        }]);

})();
