(function() {
    'use strict';

    angular.module('BlurAdmin.sides', [])
        .config(['$stateProvider', function($stateProvider){
            $stateProvider
                .state('dashboard', {
                    url: '/dashboard',
                    templateUrl: 'app/pages/sides/sides.html',
                    data:{
                        permissions:{
                            only: ['AUTHORIZED']
                        }
                    },
                    params:{ defaultState:'' }
                }).state('dashboard.home', {
                    url: '/home',
                    templateUrl: 'app/pages/home/home.html',
                    title: 'Principal',
                    controller: "HomeCtrl",
                    sidebarMeta: {
                        icon: 'fa fa-home',
                        order: 100,
                    },
                    data:{
                        permissions:{
                            only: ['ADMIN']
                        }
                    }
                });
        }]);
})();
