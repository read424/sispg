(function() {
    'use strict';
    angular.module('BlurAdmin.pages', [
            'ui.router',
            'BlurAdmin.pages.home',
            'BlurAdmin.pages.catalogo',
            'BlurAdmin.pages.compras',
            'BlurAdmin.pages.rrhh',
            'BlurAdmin.pages.inventario',
            'BlurAdmin.pages.ventas',
            'BlurAdmin.pages.reportes',
            'BlurAdmin.pages.seguridad',
            'BlurAdmin.pages.configuracion'
        ])
        .filter('summaryTotal', [ '$filter', function($filter){
          return function(input, cantidad, precio, decimal){
            var output=0;
            if(angular.isArray(input)){
              angular.forEach(input, function(item){
                console.log(item);
                console.log(cantidad, ': ',item[cantidad], precio, ': ',item[precio]);
                output+=parseInt(item[cantidad])*parseFloat(item[precio]);
              });
            }
            return $filter('number')(output, decimal);
          }
        }])
        .factory('servFactory', [ '$filter', function($filter){
          return {
            aplicar_porc:function(mto_compra, mto_porcentaje, decimal){
              var mto=parseFloat(mto_compra);
              var mto_porc=parseFloat(mto_porcentaje);
              var calculo=parseFloat(mto)+(parseFloat(mto)*parseFloat(mto_porc)/100);
              return calculo.toFixed(decimal || 2);
          },
            calularNeto:function(monto, excento_impuesto, porc_impuesto){
              var monto=parseFloat(monto) || 0.00;
              var monto_neto=monto;
              if(!excento_impuesto){//
                monto_neto=monto/(1+(parseFloat(porc_impuesto)/100));
              }
              return monto_neto.toFixed(2);
            }
          }
        }])
        .factory('serviceAPI', [ '$http', 'apiEndpoint', function($http, apiEndpoint){
          return {
            getPrinters:function(params){ return $http.get(apiEndpoint+'impresoras.php?oper=listar'); },
            existCargo:function(params){ return $http.post(apiEndpoint+'cargo.php?oper=existe', params); },
            getCondicionPago:function(){ return $http.get(apiEndpoint+'condicionpago.php?oper=listar');},
            existFactCompra:function(params){ return $http.post(apiEndpoint+'compras.php?oper=existeFactura', params); },
            guardarCompra:function(params){ return $http.post(apiEndpoint+'compras.php?oper=guardar', params); },
            infoEmpresa:function(){ return $http.get(apiEndpoint+'empresa.php'); },
            guardarEmpresa:function(params){ return $http.post(apiEndpoint+'empresa.php?oper=guardar', params)},
            changeDefaultModena:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=moneda_predeterminada', params); },
            existMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=existMoneda', params); },
            existSimbMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=existSimbolo', params); },
            getMonedas:function(){ return $http.get(apiEndpoint + 'monedas.php?oper=listar'); },
            cambiarStatusMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=status', params); },
            getMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=consultar', params); },
            guardarMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=guardar', params); },
            eliminarMoneda:function(params){ return $http.post(apiEndpoint+'monedas.php?oper=eliminar', params); },
            statusBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=status', params); },
            trashBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=eliminar', params); },
            getBancos:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=listar', params); },
            existsBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=existnombre', params); },
            consultarBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=consultar', params); },
            existSiglaBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=existSigla', params); },
            guardarBanco:function(params){ return $http.post(apiEndpoint+'bancos.php?oper=guardar', params); },
            statusNumCtaBancaria:function(params){ return $http.post(apiEndpoint+'numctasbancos.php?oper=status', params); },
            eliminarCtaBancaria:function(params){ return $http.post(apiEndpoint+'numctasbancos.php?oper=eliminar', params); },
            existCtaBancaria:function(params){ return $http.post(apiEndpoint+'numctasbancos.php?oper=existNumcta', params); },
            getCuentasBancarias:function(params){ return $http.post(apiEndpoint+'numctasbancos.php?oper=listar', params); },
            guardarCtaBancaria:function(params){ return $http.post(apiEndpoint+'numctasbancos.php?oper=guardar', params); },
            eliminarTCta:function(params){ return $http.post(apiEndpoint+'tipoctabanco.php?oper=eliminar', params); },
            cambiarStatusTCta:function(params){ return $http.post(apiEndpoint+'tipoctabanco.php?oper=status', params); },
            existTipoCta:function(params){ return $http.post(apiEndpoint+'tipoctabanco.php?oper=existDescripcion', params); },
            getTipoctas:function(){ return $http.get(apiEndpoint+'tipoctabanco.php?oper=listar'); },
            consultarTipocta:function(params){ return $http.post(apiEndpoint+'tipoctabanco.php?oper=consultar', params); },
            guardarTipocta:function(params){ return $http.post(apiEndpoint+'tipoctabanco.php?oper=guardar', params); },
            existsNomPais:function(params){ return $http.post(apiEndpoint+'paises.php?oper=existPais', params) },
            getPaises:function(params){ return $http.post(apiEndpoint+'paises.php?oper=listar', params); },
            getPais:function(params){ return $http.post(apiEndpoint+'paises.php?oper=consultar', params); },
            guardarPais:function(params){ return $http.post(apiEndpoint+'paises.php?oper=guardar', params); },
            borrarPais:function(params){ return $http.post(apiEndpoint+'paises.php?oper=eliminar', params); },
            asignDefaultpais:function(params){ return $http.post(apiEndpoint+'paises.php?oper=predeterminado', params); },
            existNomDpto:function(params){ return $http.post(apiEndpoint+'estados.php?oper=existDpto', params); },
            getDepPais:function(params){ return $http.post(apiEndpoint+'estados.php?oper=listar', params); },
            guardarDptoPais:function(params){ return $http.post(apiEndpoint+'estados.php?oper=guardar', params); },
            consultarDptoPais:function(params){ return $http.post(apiEndpoint+'estados.php?oper=consultar', params); },
            statusDptoPais:function(params){ return $http.post(apiEndpoint+'estados.php?oper=status', params); },
            borrarDptoPais:function(params){ return $http.post(apiEndpoint+'estados.php?oper=eliminar', params); },
            existTipDocumento:function(params){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=existTipDoc', params); },
            gettipdocpred:function(){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=existPredet'); },
            getTiposDocumentos:function(){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=listar'); },
            guardarTipDocumento:function(params){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=guardar', params); },
            constularTipDocumento:function(params){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=consultar', params); },
            statusTipDocumento:function(params){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=status', params); },
            eliminarTipDocumento:function(params){ return $http.post(apiEndpoint+'tipodocumentos.php?oper=eliminar', params); },
            existGrupo:function(params){ return $http.post(apiEndpoint+'grupos.php?oper=existnombre', params); },
            existCodeGrupo:function(params){ return $http.post(apiEndpoint+'grupos.php?oper=existCode', params); },
            getGrupos:function(){ return $http.post(apiEndpoint+'grupos.php?oper=listar'); },
            addGrupos:function(params){ return $http.post(apiEndpoint+'grupos.php?oper=guardar', params); },
            consultarGrupo:function(paramas){ return $http.post(apiEndpoint+'grupos.php?oper=consultar', paramas); },
            existsMarca:function(params){ return $http.post(apiEndpoint+'laboratorios.php?oper=existnombre', params); },
            getMarcas:function(params){ return $http.post(apiEndpoint+'laboratorios.php?oper=listar', params); },
            consultarMarca:function(params){ return $http.post(apiEndpoint+'laboratorios.php?oper=consultar', params); },
            addMarcas:function(data){ return $http.post(apiEndpoint+'laboratorios.php?oper=guardar', data); },
            exitsSubgrupo:function(params){ return $http.post(apiEndpoint+'subgrupos.php?oper=existSubgrupo', params); },
            getSubGrupos:function(data){ return $http.post(apiEndpoint+'subgrupos.php?oper=listar', data); },
            consultarSubgrupo:function(params){ return $http.post(apiEndpoint+'subgrupos.php?oper=consultar', params); },
            addSubgrupo:function(data){ return $http.post(apiEndpoint+'subgrupos.php?oper=guardar', data); },
            existNomLinea:function(params){ return $http.post(apiEndpoint+'lineas.php?oper=existnombre', params); },
            getLineas:function(params){ return $http.post(apiEndpoint+'lineas.php?oper=listar', params); },
            consultarLinea:function(params){ return $http.post(apiEndpoint+'lineas.php?oper=consultar', params); },
            addLineas:function(data){ return $http.post(apiEndpoint+'lineas.php?oper=guardar', data); },
            getConfig:function(){ return $http.get(apiEndpoint+'configuracion.php'); },
            guardarConfig:function(paramas){ return $http.post(apiEndpoint+'configuracion.php?oper=guardar', paramas)},
            signIn:function(data){ return $http.post(apiEndpoint+'signin.php', data); },
            existUMedida:function(params){ return $http.post(apiEndpoint+'unidadmedidas.php?oper=existnombre', params); },
            exitSiglaUMedida:function(params){ return $http.post(apiEndpoint+'unidadmedidas.php?oper=existAbrev', params); },
            getUMedidas:function(){ return $http.get(apiEndpoint+'unidadmedidas.php?oper=listar'); },
            consultarUMedida:function(params){ return $http.post(apiEndpoint+'unidadmedidas.php?oper=consultar', params); },
            addUMedida:function(data){ return $http.post(apiEndpoint+'unidadmedidas.php?oper=guardar', data); },
            addInvinicial:function(params){ return $http.post(apiEndpoint+'productos.php?oper=invinicial', params); },
            existNomProducto:function(params){ return $http.post(apiEndpoint+'productos.php?oper=existnombre', params); },
            existeCodeProducto:function(params){ return $http.post(apiEndpoint+'productos.php?oper=existcodigo', params); },
            getProductos:function(params){ return $http.post(apiEndpoint+'productos.php?oper=listar', params); },
            getProducto:function(data){ return $http.post(apiEndpoint+'productos.php?oper=consultar', data); },
            getProductoCode:function(params){ return $http.post(apiEndpoint+'productos.php?oper=codigo', params); },
            consultarStock:function(params){ return $http.post(apiEndpoint+'productos.php?oper=consultarstock',params); },
            guardarProducto:function(data){ return $http.post(apiEndpoint+'productos.php?oper=guardar', data); },
            searchProducto:function(params){ return $http.post(apiEndpoint+'productos.php?oper=search', params); },
            seekCodeProduct:function(params){ return $http.post(apiEndpoint+'productos.php?oper=foundcode', params)},
            getAlmacenes:function(){ return $http.get(apiEndpoint+'almacenes.php?oper=listar'); },
            getUbicaciones:function(){ return $http.get(apiEndpoint+'ubicaciones.php?oper=listar'); },
            existRucProveedor:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=existeRuc', params); },
            existDocumProveedor:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=existDocFiscal', params); },
            getProveedores:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=listar', params); },
            guardarProveedor:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=guardar', params); },
            getProveedor:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=consultar', params); },
            searchRUC:function(params){ return $http.post(apiEndpoint+'proveedor.php?oper=buscarruc', params); },
            guardarOrdenPedido:function(params){ return $http.post(apiEndpoint+'notaspedido.php?oper=guardar', params); },
            listOrdenescompra:function(){ return $http.get(apiEndpoint+'notascompra.php?oper=listar'); },
            getListClients:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=listar', params); },
            getClientsID:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=consultarid', params); },
            getClientND:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=consultardocument', params); },
            getClientID:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=consultar', params); },
            existClient:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=existe', params); },
            guardarClient:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=guardar', params); },
            seekDocumentoClient:function(params){ return $http.post(apiEndpoint+'clientes.php?oper=founddocumento', params)},
            addVentaTMP:function(params){ return $http.post(apiEndpoint+'ventas.php?oper=addTMP', params); },
            delVentaTMP:function(params){ return $http.post(apiEndpoint+'ventas.php?oper=delTMP', params); },
            deleteVentaTMP:function(){ return $http.get(apiEndpoint+'ventas.php?oper=closeVentas'); },
            addBolet:function(params){ return $http.post(apiEndpoint+'ventas.php?oper=guardar', params); },
            getTipopagos:function(){ return $http.get(apiEndpoint+'tipospagos.php?oper=listar'); },
            existRolUsuario:function(params){ return $http.post(apiEndpoint+'roles.php?oper=existe', params); },
            guardarRolUsuario:function(params){ return $http.post(apiEndpoint+'roles.php?oper=guardar', params); },
            consultarRolUsuario:function(params){ return $http.post(apiEndpoint+'roles.php?oper=consultar', params); },
            getRoles:function(){ return $http.get(apiEndpoint+'roles.php?oper=listar'); },
            deleteRol:function(params){ return $http.post(apiEndpoint+'roles.php?oper=eliminar', params)},
            cambiarStatusRol:function(params){ return $http.post(apiEndpoint+'roles.php?oper=changestatus', params); },
            getUsuarios:function(){return $http.get(apiEndpoint+'usuarios.php?oper=listar'); },
            exitUsuario:function(params){ return $http.post(apiEndpoint+'usuarios.php?oper=existUser', params); },
            guardarUsuario:function(params){ return $http.post(apiEndpoint+'usuarios.php?oper=guardar', params); },
            consultarUsuario:function(params){ return $http.post(apiEndpoint+'usuarios.php?oper=consultar', params); },
            existdocEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=existdocumento', params); },
            validDocmEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=validdocumento', params);},
            getEmpleados:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=listar', params); },
            consultarEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=consultar', params); },
            infoEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=info', params); },
            guardarEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=guardar', params); },
            validEmailEmpleado:function(params){ return $http.post(apiEndpoint+'empleados.php?oper=validemail', params); },
            statusEmpleado: function(params){ return $http.post(apiEndpoint+'empleados.php?oper=status', params); },
            hiddenEmpleado: function(params){ return $http.post(apiEndpoint+'empleados.php?oper=visibility', params); },
            listCompras:function(){ return $http.post(apiEndpoint+'compras.php?oper=listar'); },
            detailsFactura:function(params){ return $http.post(apiEndpoint+'compras.php?oper=detallefactura', params); },
            addProximopago:function(params){ return $http.post(apiEndpoint+'compras.php?oper=nextpago', params); },
            getCVentasperdidas:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=listar', params); },
            existeConceptPerdida:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=existconcepto', params); },
            consultarCVPerdida:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=consultar', params); },
            guardarCVPerdida:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=guardar', params); },
            statusCVPerdida:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=status', params); },
            trashCVPerdida:function(params){ return $http.post(apiEndpoint+'conceptoventasperdidas.php?oper=eliminar', params); }
          }
          }]);
})();
